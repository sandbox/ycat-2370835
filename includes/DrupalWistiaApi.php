<?php

/**
 * @file
 *
 */

include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'media_wistia') . '/includes/WistiaApi.class.php';

class DrupalWistiaApi extends WistiaApi {

  protected function __send($url) {

    $username = 'api';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERPWD, $username . ':'. $this->apiKey);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $proxy_server = variable_get('proxy_server', '');

    if ($proxy_server) {
      $proxy = variable_get('proxy_server', '') . ':' . variable_get('proxy_port', 8080);
      curl_setopt($ch, CURLOPT_PROXY, $proxy);
    }
    else {
      curl_setopt($ch, CURLOPT_PROXY, null);
    }

    $result = curl_exec($ch);
    curl_close($ch);
    $this->response = $result;

    return $result;
  }

}
