<?php

/**
 * @file
 * Default files (wistia://) stream wrapper class.
 */

/**
 * Media Wistia stream wrapper class.
 */
class MediaWistiaStreamWrapper extends MediaReadOnlyStreamWrapper {

  protected $api;
  protected $base_url = 'http://home.wistia.com/medias/';

  public function __construct() {
    $this->api = new DrupalWistiaApi(variable_get('media_wistia_api_key'));
  }

  /**
   * Overrides interpolateUrl() defined in MediaReadOnlyStreamWrapper.
   * Checks to see if the video is in our cache, if not we request from the api.
   */
  function interpolateUrl() {
    $parameters = $this->get_parameters();
    if (!empty($parameters['v'])) {
      return $this->base_url . $parameters['v'];
    }
  }

  /**
   * Returns the wistia mimetype.
   * @param $uri
   * @param null $mapping
   * @return string
   */
  static function getMimeType($uri, $mapping = NULL) {
    return 'video/wistia';
  }

  /**
   * Gets the original thumbnail for the Wistia video
   * @return mixed
   */
  function getOriginalThumbnailPath($http_path = '') {
    $parts = $this->get_parameters();

    if (!empty($parts['v']) && $this->api->hasApiKey()) {
      $media = $this->api->mediaShow($parts['v']);
      $url = $media->thumbnail->url;
    }
    else {
      if(isset($parts['url'])) {
        $media = _media_wistia_request_data($parts['url']);
        $url = $media['thumbnail_url'];
      }
      else {
        $media = _media_wistia_request_data($this->base_url . $parts['v']);
        $url = $media['thumbnail_url'];
      }
    }
    return $url;
  }

  /**
   * Returns the url to the thumbnail for the Wistia video
   * @return string
   */
  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = file_default_scheme() . '://media-wistia/' . check_plain($parts['v']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

      $response = drupal_http_request($this->getOriginalThumbnailPath());

      if (!isset($response->error)) {
        file_unmanaged_save_data($response->data, $local_path, TRUE);
      }
      else {
        @copy($this->getOriginalThumbnailPath(), $local_path);
      }
    }
    return $local_path;
  }

  function mediaWistiaParseHttpUrl($url) {
    $parts = explode("//v/", $url);

    return $parts[1];
  }

  function parse($embedCode) {
    $patterns = array(
      '/https?:\/\/(.+)?(wistia\.com|wi\.st)\/(medias|embed)\/(.*)/',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      if (isset($matches[4])) {
        return $matches[4];
      }
    }
  }
}
